resource "aws_vpc" "final" {
  cidr_block           = "10.0.0.0/24"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "final_vpc"
  }
}

resource "aws_subnet" "subnet" {
  cidr_block        = cidrsubnet(aws_vpc.final.cidr_block, 3, 1)
  vpc_id            = aws_vpc.final.id
  availability_zone = "us-east-1a"
}

resource "aws_internet_gateway" "final_gw" {
  vpc_id = aws_vpc.final.id
}

resource "aws_route_table" "final_rt" {
  vpc_id = aws_vpc.final.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.final_gw.id
  }
}

resource "aws_route_table_association" "final_assoc" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.final_rt.id
}

resource "aws_security_group" "final_group" {
  name = "allow-ssh-from-anywhere"

  vpc_id = aws_vpc.final.id

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = file("smallvpc_keys.pub")
}


resource "aws_instance" "final_ec2" {
  count                       = 1
  ami                         = "ami-07d9b9ddc6cd8dd30"
  instance_type               = "t2.medium"
  key_name                    = "deployer-key"
  security_groups             = [aws_security_group.final_group.id] #removing the quotes and $ stopped it from making duplicates of everything
  associate_public_ip_address = true

  subnet_id = aws_subnet.subnet.id
  user_data = file("${path.module}/setup.sh")
  tags = {
    Name = var.instance_tag2[count.index]
  }
}
