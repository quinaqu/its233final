#!/bin/bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh ./get-docker.sh 

mkdir certs
cd certs
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 3650 -nodes -subj "/C=XX/ST=StateName/L=CityName/O=CompanyName/OU=CompanySectionName/CN=CommonNameOrHostname"

sudo service docker start

##sudo docker run --detach \
#    --name nginx-proxy \
#    --publish 80:80 \
#    --volume /home/ubuntu/certs:/etc/nginx/certs \
#    --volume /var/run/docker.sock:/tmp/docker.sock:ro \
#    nginxproxy/nginx-proxy:1.5

sudo docker run --publish 80:5000 --detach quinaquan/connectnow:latest

##updated thing